# Interpolation de chaîne

Vous pouvez placer une variable entre les caractères { et } pour indiquer à C# de remplacer ce texte par la valeur de la variable.
Si vous ajoutez un $ avant le guillemet ouvrant de la chaîne, vous pouvez ajouter des variables, comme aFriend, entre accolades dans la chaîne.

string aFriend = "Bane";
Console.WriteLine($"Hello {aFriend}"); //Console.WriteLine("Hello {0}", aFriend); font la même chose

# Création et compilation d'un projet en ligne de commande

dotnet new console -n NumbersInCSharp -o .    //Création d'un projet dans le repertoire courant 
//dotnet new console
dotnet run //Exécution du programme. 

# Listes

var names = new List<string> { "<name>", "Ana", "Felipe" };
//Parcours de la liste
foreach (var name in names)
{
  Console.WriteLine($"Hello {name.ToUpper()}!");
}

# Ajouter un modèle à une application de pages Razor dans ASP.NET Core

EF Core est un mapper relationnel objet (O/RM) qui simplifie l’accès aux données.
Les classes de modèle portent le nom de classes OCT (« Objet CLR Traditionnel »), car elles n’ont pas de dépendances envers EF Core. Elles définissent les propriétés des données stockées dans la base de données. src : https://docs.microsoft.com/fr-fr/aspnet/core/tutorials/razor-pages/model?view=aspnetcore-3.1&tabs=visual-studio


## Team Foundation Server TFS s'appelle désormais Azure DevOps Server
## Visual Studio Team Services (VSTS) en Azure DevOps Services


# Utilisation de la base de données dans modèle MVC

- Installation d'Entity Framework dans Console du Gestionnaire du package : 
Install-Package Microsoft.EntityFrameworkCore.SqlServer

- Création d'une classe de contexte de la base de données : Une classe de contexte de base de données est necéssaire pour coordonner les fonctionnalités d'EF Core (CRUD) pour le modèle Movie

- Inscrire le contexte de la base de données dans Startup.cs

- Ajout de la chaîne de connexion de la bdd dans appSettings.json

- Génération automatique des pages du model avec le scalfolding (controlleur et vue) (optionnel)

- Migration initiale : A ce stade la base de données n'est pas encore crééé, il faut faire une prémière migration dans la console du gestionnaire du package. La fonctionnalité Migration est un ensemble d'outils qui vous permettent de créer et de mettre à jour une base de données pour qu'elle corresponde à votre modèle de données.
Add-Migration InitialCreate
Update-Database

# API Web

